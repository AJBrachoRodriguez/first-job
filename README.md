# Data Governance

![Transforming the data into pipelines](img/Background_picture__LinkedIn_.png)

> This is how the data changes the world!\newline

[![LinkedIn](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/alexangel-bracho-m-sc-in-physics-510675239/)

[![Medium](https://img.shields.io/badge/Medium-12100E?style=for-the-badge&logo=medium&logoColor=white)](https://medium.com/@alexangelb)

[![GitHub](https://img.shields.io/badge/GitHub-82100E?style=for-the-badge&logo=medium&logoColor=orange)](https://medium.com/@alexangelb)

Here we´ll see how a **Data Governance** works, including all the steps involved in the development and quality.

<h5><strong>Note<strong><h5>
> If you do it in VSCode would be better.

### First Step
 
Firstly, we looked for the data in the website.
![First Step](img/MiniProject_Loans.png)

### Second Step

Secondly, we prepared the data.

### Third Step

Then, we analyzed the insights.


## Name
Data Governance

## About Us

In the following link you´ll find more information.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
